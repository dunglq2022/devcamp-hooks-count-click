import { useEffect, useState } from "react";

function Count () {
    const [count, setCount] = useState(0)

    const increaseCount = () => {
        console.log('increase count!');
        setCount(count + 1);
    };

    useEffect(() => {
        console.log('componentDidUpdate!');
        document.title = `You clicked ${count} times`
    });

    useEffect(() => {
        console.log('componentDidMound!');
    }, []);

    return (
        <div className="container" style={{backgroundColor: '#1b2634', width: 500, height: 200}}>
            <h1 className="pt-4" style={{color: '#8bdfeb'}}>You clicked {count} times</h1>
            <button onClick={increaseCount} className="ms-4 mt-3" style={{backgroundColor: '#8bdfeb', width: 150, height: 60, borderColor: '#8bdfeb', borderRadius: 7, fontSize: 30}}>Click me</button>
        </div>
    )
}

export default Count;